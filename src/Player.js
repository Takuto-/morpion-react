import React, { Component } from 'react'



class Players extends Component {
    state = {
        Players : [
            {name: 'Léo', age: 18},
            {name: 'Mario', age: 20},
            {name: 'Patrick', age: 21},
            {name: 'Alex', age: 30}
        ]
    };
    render() {
        return (
            <div className="playerStyle">
            <ul>
            {this.state.Players.map((player) =>(
                <li key={player}>Salut {player.name}</li> 
            ))}
            </ul>  
            </div>
        )
    }
}

export default Players;


