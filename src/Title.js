import React, { Component } from 'react'
import coggers from './img.png'



class Title extends Component {

    addStyle = (e) => {

        if (e.target.classList.contains('styled')) {

            e.target.classList.remove('styled');
        } else {
            e.target.classList.add('styled')
        }

    }

    render() {
        return (
            <div>
                <h1 onMouseOver={this.addStyle}>Hello world</h1>
                <img className="imgStyle" src={coggers} alt='spin to win'></img>
            </div>
        )

    }
}
export default Title